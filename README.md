# Spring Boot Data Access

It's a demo app that uses Spring Data access.
It uses H2 database in in-memory mode. The settings and the database related queries are in the following files:
#
* src\test\resources\application.properties
* src\test\resources\schema.sql
* src\test\resources\data.sql

It uses JUnit to verify the functionality..

## The steps to be followed are: ##
* git clone https://rajibpsarma@bitbucket.org/rajibpsarma/spring_boot_data_access_demo.git
* cd spring_boot_data_access_demo
* mvn clean test