package org.bitbucket.rajibpsarma.dataaccess.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="department")
public class Department {
	@Id
	private long id;
	
	private String name;
	
	private String description;
	
	@OneToOne
	private Employee employee;
}
