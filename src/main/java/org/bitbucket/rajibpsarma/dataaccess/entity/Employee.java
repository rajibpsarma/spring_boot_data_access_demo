package org.bitbucket.rajibpsarma.dataaccess.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="employee")
public class Employee {
	@Id
	private long id;
	
	private String name;
	
	private String address;
	
	@OneToOne
	@JoinColumn(name = "dept_id")
	private Department department;
}
