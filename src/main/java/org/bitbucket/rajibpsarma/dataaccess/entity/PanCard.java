package org.bitbucket.rajibpsarma.dataaccess.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="pan_card")
public class PanCard {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@Column(name="number")
	private String number;
	
	@Column(name="issued_by")
	private String issuedBy;
	
	@OneToOne(mappedBy = "panCard", cascade = CascadeType.ALL)
	private TaxPayer taxPayer;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getIssuedBy() {
		return issuedBy;
	}

	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}
	
	public TaxPayer getTaxPayer() {
		return taxPayer;
	}

	public void setTaxPayer(TaxPayer taxPayer) {
		this.taxPayer = taxPayer;
	}

	public PanCard() {}

	public PanCard(String number, String issuedBy) {
		this.number = number;
		this.issuedBy = issuedBy;
	}
}