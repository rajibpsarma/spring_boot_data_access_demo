package org.bitbucket.rajibpsarma.dataaccess.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="person")
public class Person {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="name")
	private String name;
	
	//@Column(name="address")
	// For the same name, @Column is not needed.
	private String address;
	
	@Column(name="pin")
	private int pin;
	
	@Column(name="age")
	private int age;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public Person() {}

	public Person(String name, String address, int pin, int age) {
		this.name = name;
		this.address = address;
		this.pin = pin;
		this.age = age;
	}
	
	public String toString() {
		return "{"+id + ", " + name + ", " + address + ", " + pin + ", " + age+"}";
	}
}
