package org.bitbucket.rajibpsarma.dataaccess.repository;

import org.bitbucket.rajibpsarma.dataaccess.entity.Department;
import org.springframework.data.repository.CrudRepository;

public interface DepartmentRepository extends CrudRepository<Department, Long> {

}
