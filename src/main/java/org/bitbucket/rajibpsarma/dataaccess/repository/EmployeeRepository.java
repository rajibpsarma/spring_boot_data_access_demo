package org.bitbucket.rajibpsarma.dataaccess.repository;

import org.bitbucket.rajibpsarma.dataaccess.entity.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}
