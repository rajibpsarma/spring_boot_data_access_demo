package org.bitbucket.rajibpsarma.dataaccess.repository;

import org.bitbucket.rajibpsarma.dataaccess.entity.PanCard;
import org.springframework.data.repository.CrudRepository;

public interface PanCardRepository extends CrudRepository<PanCard, String> {
	public PanCard getPanCardByNumber(String panCardNumber);
}
