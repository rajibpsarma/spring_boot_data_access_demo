package org.bitbucket.rajibpsarma.dataaccess.repository;

import java.util.List;

import org.bitbucket.rajibpsarma.dataaccess.entity.Person;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
	public List<Person> getPersonByName(String name);
	// Sort dynamically
	public List<Person> getPersonByName(String name,Sort sort);	
	public List<Person> getPersonByNameIgnoreCase(String name);
	public List<Person> getPersonByNameContainingIgnoreCase(String name);
	public List<Person> getPersonByAddress(String address);
	public List<Person> getPersonByPin(int pin);
	public List<Person> getPersonByNameAndPin(String name, int pin);
	public List<Person> getPersonByNameOrderByPinDesc(String name);
	public List<Person> getPersonByAgeLessThan(int age);
	// Delete persons based on name
	public long deletePersonByName(String name);
}
