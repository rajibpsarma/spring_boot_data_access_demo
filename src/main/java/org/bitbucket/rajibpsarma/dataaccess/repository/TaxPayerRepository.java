package org.bitbucket.rajibpsarma.dataaccess.repository;

import org.bitbucket.rajibpsarma.dataaccess.entity.TaxPayer;
import org.springframework.data.repository.CrudRepository;

public interface TaxPayerRepository extends CrudRepository<TaxPayer, Long> {
	public TaxPayer getTaxPayerByName(String taxPayerName);
}
