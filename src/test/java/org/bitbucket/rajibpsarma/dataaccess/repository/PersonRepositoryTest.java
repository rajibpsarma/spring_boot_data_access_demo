package org.bitbucket.rajibpsarma.dataaccess.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import javax.transaction.Transactional;

import org.bitbucket.rajibpsarma.dataaccess.entity.Person;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class PersonRepositoryTest {
	
	@Autowired
	private PersonRepository repo;
	
	@Test
	public void testGetPersonByName() {
		try {
			// Find person with name "Rajib Sarma". Should get 2.
			List<Person> list = repo.getPersonByName("Rajib Sarma");
			if(2 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}
			
			// Find person with name "rajib sarma". Should get 0.
			list = repo.getPersonByName("rajib sarma");
			if(0 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}			
		}
		catch(Exception ex) {
			assertTrue(false, "Error while invoking method");
		}
	}
	
	@Test
	public void testGetPersonIgnoreCaseByName() {
		try {
			// Find person with name "rajib sarma". Should get 2.
			List<Person> list = repo.getPersonByNameIgnoreCase("rajib sarma");
			if(2 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}		
		}
		catch(Exception ex) {
			assertTrue(false, "Error while invoking method");
		}
	}	
	
	@Test
	public void testGetPersonByAddress() {
		try {
			// Find person with address "Kundanhalli Gate"
			List<Person> list = repo.getPersonByAddress("Kundanhalli Gate");
			if(2 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}
		}
		catch(Exception ex) {
			assertTrue(false, "Error while invoking method");
		}
	}	
	
	@Test
	public void testGetPersonByPin() {
		try {
			// Find person with pin "560037"
			List<Person> list = repo.getPersonByPin(560037);
			if(2 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}
			
			// Find person with pin "780032"
			list = repo.getPersonByPin(780032);
			if(2 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}		
		}
		catch(Exception ex) {
			assertTrue(false, "Error while invoking method");
		}
	}	
	
	@Test
	public void testGetPersonByNameAndPin() {
		try {
			// Find person with pin "560037" and name "Rajib Sarma"
			List<Person> list = repo.getPersonByNameAndPin("Rajib Sarma", 780032);
			if(1 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}	
		}
		catch(Exception ex) {
			assertTrue(false, "Error while invoking method");
		}
	}
	
	@Test
	public void testGetPersonByNameOrderByPinDesc() {
		try {
			List<Person> list = repo.getPersonByNameOrderByPinDesc("Rajib Sarma");
			if(2 == list.size()) {
				assertTrue(true);
				// Now, verify the order
				Person p = null;
				for(int i=0 ; i<list.size(); i++) {
					p = list.get(i);
					if((i == 0) && (780032 == p.getPin())) {
						assertTrue(true);
					} else if ((i == 1) && (560037 == p.getPin())) {
						assertTrue(true);
					} else {
						fail();
					}
				}
			} else {
				assertTrue(false);
			}	
		}
		catch(Exception ex) {
			assertTrue(false, "Error while invoking method");
		}		
	}
	
	@Test
	public void testGetPersonByAgeLessThan() {
		try {
			List<Person> list = repo.getPersonByAgeLessThan(21);
			if(3 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}	
			
			list = repo.getPersonByAgeLessThan(18);
			if(1 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}			
		}
		catch(Exception ex) {
			assertTrue(false, "Error while invoking method");
		}
	}
	
	@Test
	public void testGetPersonByNameContainingIgnoreCase() {
		try {
			List<Person> list = repo.getPersonByNameContainingIgnoreCase("rajib");
			if(2 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}	
			
			list = repo.getPersonByNameContainingIgnoreCase("Sarma");
			if(4 == list.size()) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}			
		}
		catch(Exception ex) {
			assertTrue(false, "Error while invoking method");
		}
	}
	
	@Test
	public void testGetPersonByNameDynamicSorting() {
		try {
			// Sort by Age ASC
			List<Person> list = repo.getPersonByName("Rajib Sarma", Sort.by(Sort.Direction.ASC, "age"));
			if(2 == list.size()) {
				assertTrue(true);
				// Now, verify the order
				Person p = null;
				for(int i=0 ; i<list.size(); i++) {
					p = list.get(i);
					if((i == 0) && (20 == p.getAge())) {
						assertTrue(true);
					} else if ((i == 1) && (21 == p.getAge())) {
						assertTrue(true);
					} else {
						fail();
					}
				}
			} else {
				assertTrue(false);
			}	
			
			// Sort by address DESC
			list = repo.getPersonByName("Rajib Sarma", Sort.by(Sort.Direction.DESC, "address"));
			if(2 == list.size()) {
				assertTrue(true);
				// Now, verify the order
				Person p = null;
				for(int i=0 ; i<list.size(); i++) {
					p = list.get(i);
					if((i == 0) && (p.getAddress().equals("Rampur"))) {
						assertTrue(true);
					} else if ((i == 1) && (p.getAddress().startsWith("Kundanhalli"))) {
						assertTrue(true);
					} else {
						fail();
					}
				}
			} else {
				assertTrue(false);
			}				
		}
		catch(Exception ex) {
			ex.printStackTrace();
			assertTrue(false, "Error while invoking method");
		}
	}
	
	// Tests the insert Person functionality
	@Test
	@Order(1)
	public void testInsertPerson() {
		try {
			// Add a new Person that is not already available in db
			String name = "Biman Bora";
			
			// check that it's not available
			List<Person> list = repo.getPersonByName(name);
			if(list.size() == 0) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}
			
			// Now, insert
			Person p = new Person(name, "Maligaon", 780009, 22);
			repo.save(p);
			
			// Now, check again
			list = repo.getPersonByName(name);
			if(list.size() == 1) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			fail();
		}
	}
	
	// Tests the update Person functionality
	@Test
	@Transactional
	@Order(2)
	public void testUpdatePerson() {
		try {
			String name = "Biman Bora";
			Person biman = null;
			long personId = 0;
			
			// check that it's available
			List<Person> list = repo.getPersonByName(name);
			if(list.size() == 1) {
				assertTrue(true);
				biman = list.get(0);
				if(biman.getAddress().equals("Maligaon")) {
					assertTrue(true);
					personId = biman.getId();
				} else {
					assertTrue(false);
				}
			} else {
				assertTrue(false);
			}
		
			// Now, update
			biman = null;
			biman = repo.findById(personId).get();
			biman.setAddress("Nagaon");
			repo.save(biman);
			
			// Now, fetch and check again
			list = null; biman = null;
			List<Person> list1 = repo.getPersonByName(name);
			if(list1.size() == 1) {
				assertTrue(true);
				Person p = list1.get(0);
				if(p.getAddress().equals("Nagaon")) {
					assertTrue(true);
				} else {
					assertTrue(false);
				}
			} else {
				assertTrue(false);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			fail();
		}
	}
	
	// Tests the delete Person functionality
	@Test
	@Transactional
	@Order(3)
	public void testDeletePerson() {
		try {
			String name = "Biman Bora";
			Person biman = null;
			
			// check that the person is available
			List<Person> list = repo.getPersonByName(name);
			if(list.size() == 1) {
				assertTrue(true);
				biman = list.get(0);
			} else {
				assertTrue(false);
			}
			
			// Now, delete
			repo.deletePersonByName(name);
			// Can use the following too
			//repo.delete(biman);
			
			// Now, check again
			list = repo.getPersonByName(name);
			if(list.size() == 0) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			fail();
		}
	}	
}
