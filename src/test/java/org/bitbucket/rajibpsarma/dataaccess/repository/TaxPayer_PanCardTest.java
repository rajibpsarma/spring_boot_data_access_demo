package org.bitbucket.rajibpsarma.dataaccess.repository;

import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import javax.transaction.Transactional;

import org.bitbucket.rajibpsarma.dataaccess.entity.PanCard;
import org.bitbucket.rajibpsarma.dataaccess.entity.TaxPayer;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class TaxPayer_PanCardTest {
	@Autowired
	private TaxPayerRepository taxPayerRepo;
	
	@Autowired
	private PanCardRepository panCardRepo;

	@Test
	@Transactional
	public void testInsertFunctionality() {
		try {
			String panCardNo = "kpidf3983b";
			String issueAuthority = "XYZ Authority";
			String taxPayerName = "Rajib P Sarma";
			
			/*
			// Create
			PanCard panCard = new PanCard(panCardNo, issueAuthority);
			TaxPayer taxPayer = new TaxPayer(taxPayerName, "Bangalore, Karnataka");
			taxPayer.setPanCard(panCard);
			panCard.setTaxPayer(taxPayer);
			//taxPayerRepo.save(taxPayer);
			panCardRepo.save(panCard);
			*/
			
			/*
			// Verify Pan card creation
			PanCard panCardNew = panCardRepo.getPanCardByNumber(panCardNo);
			if(panCardNew != null) {
				String auth = panCardNew.getIssuedBy();
				if(issueAuthority.equals(auth)) {
					assertTrue(true);
				} else {
					fail();
				}
			} else {
				fail();
			}
			
			// Verify Tax payer creation
			*/
		} catch(Exception ex) {
			ex.printStackTrace();
			fail();
		}
	}
}
