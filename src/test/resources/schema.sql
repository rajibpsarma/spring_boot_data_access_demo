-- A tabel that had no relationship with other tables
create table person (
	id bigint auto_increment primary key,
	name varchar(25) not null,
	address varchar(50),
	pin int not null,
	age int
);

-- Tax payer vs Pan card tables having one-to-one relationship
create table pan_card (
	id bigint auto_increment primary key,
	number varchar(25) unique,
	issued_by varchar(50)
);
create table tax_payer (
	id bigint auto_increment primary key,
	name varchar(25) not null,
	address varchar(50),
	pan_card_id bigint,
	
	foreign key (pan_card_id) references pan_card(id)
);

